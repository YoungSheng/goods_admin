import { basePermissions } from '@/settings'
import api from '@/api'
import { useAuthStore } from '@/store'

export async function getUserInfo() {
  const userId = localStorage.getItem('userId')
  try {
    const res = await api.getUser({ id: userId })
    const data = res.data
    const { id, username, profile, roles, currentRole } = res.data || {}
    return {
      id,
      username: data.contact,
      avatar: data.imgUrl,
      nickName: data.nickName || profile?.nickName,
      gender: data.gender || profile?.gender,
      address: data.contactMobile || profile?.address,
      email: data.email || profile?.email,
      userType: data.userType,
      roles: [
        {
          id: 1,
          code: 'SUPER_ADMIN',
          name: '超级管理员',
          enable: true,
        },
        {
          id: 2,
          code: 'ROLE_QA',
          name: '质检员',
          enable: true,
        },
      ],
      currentRole: {
        id: 1,
        code: 'SUPER_ADMIN',
        name: '超级管理员',
        enable: true,
      },
    }
  } catch (error) {
    const authStore = useAuthStore()
    authStore.logout()
  }
}

export async function getPermissions() {
  let asyncPermissions = []
  try {
    const res = await api.getRolePermissions()
    asyncPermissions = res?.data || []
  } catch (error) {
    console.error(error)
  }
  return basePermissions.concat(asyncPermissions)
}
