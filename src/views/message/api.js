import { request } from '@/utils'
import dayjs from 'dayjs'
import { NTag } from 'naive-ui'
import { NButton, NAvatar } from 'naive-ui'

export default {
  create: (data) => request.post('/messages/add', { ...data, }),
  read: (params = {}) => request.post('/messages/list', { ...params, userType: 0, }),
  update: (data) => request.post(`/messages/update`, { ...data, }),
  delete: (id) => request.post(`/messages/delete`, { id }),
  resetPwd: (id, data) => request.post(`/messages/update`, { ...data, }),
}


export const genColumns = (ctx) => {
  return [
    {
      key: 'id',
      title: 'ID',
      isTable: true,
      isForm: false,
      width: 50,
    },
    // {
    //   key: 'imgUrl',
    //   title: '图片',
    //   isTable: true,
    //   isForm: true,
    //   isImage: true,
    //   minWidth: 70,
    //   render: ({ imgUrl }) =>
    //     ctx.h(NAvatar, {
    //       size: 'medium',
    //       src: imgUrl,
    //     }),
    // },
    {
      key: 'message',
      title: '消息内容',
      isTable: true,
      isForm: true,
      minWidth: 150,
    },
    {
      key: 'createTime',
      title: '创建时间',
      isTable: true,
      isForm: false,
      render: ({ createTime }) => ctx.h('div', null, dayjs(createTime).format('YYYY-MM-DD HH:mm:ss'))
    },
    {
      title: '操作',
      key: 'actions',
      minWidth: 100,
      align: 'right',
      fixed: 'right',
      hideInExcel: true,
      isTable: true,
      render(row) {
        const h = ctx.h
        return [
          h(
            NButton,
            {
              size: 'small',
              type: 'error',
              style: 'margin-left: 12px;',
              onClick: () => ctx.handleDelete(row.id),
            },
            {
              default: () => '删除',
              // icon: () => h('i', { class: 'i-material-symbols:delete-outline text-14' }),
            }
          ),
        ]
      },
    },
  ]
}
