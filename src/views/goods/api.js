import { formatDateTime, request } from '@/utils'
import dayjs from 'dayjs'
import { NTag, NAvatar, NTable } from 'naive-ui'
import { NButton } from 'naive-ui'

export default {
  detail: (id) => request.post('/goods/detail', { id }),
  create: (data) =>
    request.post('/goods/add', {
      ...data,
      effectiveTime: formatDateTime(dayjs(data.effectiveTime || dayjs().valueOf())),
      deliveryTime: formatDateTime(dayjs(data.deliveryTime || dayjs().valueOf())),
      auctionStartTime: formatDateTime(dayjs(data.auctionStartTime || dayjs().valueOf())),
      auctionEndTime: formatDateTime((data.auctionEndTime || dayjs()).valueOf()),
    }),
  read: (params = {}) => request.post('/goods/list', params),
  update: (data) =>
    request.post(`/goods/update`, {
      ...data,
      goodsUserRelate: data.goodsUserRelate?.toString() || '',
      effectiveTime: formatDateTime(dayjs(data.effectiveTime || dayjs().valueOf())),
      deliveryTime: formatDateTime(dayjs(data.deliveryTime || dayjs().valueOf())),
      auctionStartTime: formatDateTime(dayjs(data.auctionStartTime || dayjs().valueOf())),
      auctionEndTime: formatDateTime((data.auctionEndTime || dayjs()).valueOf()),
    }),
  delete: (id) => request.post(`/goods/delete`, { id }),
  resetPwd: (id, data) =>
    request.post(`/goods/update`, {
      ...data,
      effectiveTime: formatDateTime(dayjs(data.effectiveTime || dayjs().valueOf())),
      deliveryTime: formatDateTime(dayjs(data.deliveryTime || dayjs().valueOf())),
      auctionStartTime: formatDateTime(dayjs(data.auctionStartTime || dayjs().valueOf())),
      auctionEndTime: formatDateTime((data.auctionEndTime || dayjs()).valueOf()),
    }),

  getAllRoles: () => request.get('/role?enable=1'),
}

// 'primary' | 'info' | 'success' | 'warning' | 'error'
export const goodsStatus = [
  { id: 0, name: '在售', type: 'info' },
  { id: 1, name: '售罄', type: 'error' },
]

export const statusOptions = [
  { id: 0, name: '待审核', type: 'info' },
  { id: 1, name: '审核成功', type: 'success' },
  { id: 2, name: '审核失败', type: 'error' },
]

export const genColumns = (ctx) => {
  return [
    {
      key: 'id',
      title: 'ID',
      width: 50,
      isTable: true,
      isForm: false,
    },
    {
      key: 'goodsId',
      title: '商品ID',
      isTable: false,
      isForm: false,
    },
    {
      key: 'userId',
      title: '用户ID',
      isTable: false,
      isForm: false,
    },
    {
      key: 'auctionUserId',
      title: '竞拍用户ID',
      isTable: false,
      isForm: false,
    },
    {
      key: 'name',
      title: '商品名称',
      isTable: true,
      isForm: true,
      minWidth: 180,
    },
    {
      key: 'originalPrice',
      title: '原价',
      isTable: true,
      isForm: true,
      span: 6,
      minWidth: 100,
      render: (row) => `${row.originalPrice} ${row.unit || ''}`
    },
    {
      key: 'unit',
      title: '价格单位',
      isTable: false,
      isForm: true,
      span: 6,
      minWidth: 60,
    },
    {
      key: 'number',
      title: '商品数量',
      isTable: true,
      isForm: true,
      span: 6,
      minWidth: 100,
      render: (row) => `${row.number} ${row.goodsUnit || ''}`
    },
    {
      key: 'goodsUnit',
      title: '数量单位',
      isTable: false,
      isForm: true,
      span: 6,
      minWidth: 100,
    },
    {
      key: 'sendingParty',
      title: '发拍方',
      isTable: true,
      minWidth: 140
    },
    {
      key: 'detail',
      title: '商品详情',
      isInputArea: true,
      ellipsis: {
        tooltip: true
      },
      isTable: false,
      isForm: true,
      minWidth: 220,
    },
    {
      key: 'contact',
      title: '联系人',
      isTable: false,
      isForm: true,
      minWidth: 100,
    },
    {
      key: 'contactMobile',
      title: '联系人手机号',
      isTable: false,
      isForm: true,
      minWidth: 100,
    },
    {
      key: 'bond',
      title: '保证金',
      isTable: false,
      isForm: true,
      minWidth: 60,
    },
    {
      key: 'auctionRule',
      title: '竞拍规则',
      isTable: false,
      isForm: true,
      minWidth: 110,
    },
    {
      key: 'auctionGradient',
      title: '竞价阶梯',
      isTable: false,
      isForm: true,
      minWidth: 80,
    },
    {
      key: 'delayedPeriod',
      title: '延时周期',
      isTable: false,
      isForm: true,
      minWidth: 80,
    },
    {
      key: 'imgUrl',
      title: '商品图片',
      isTable: false,
      isForm: true,
      isImage: true,
      minWidth: 100,
      render: ({ imgUrl }) =>
        h(NAvatar, {
          size: 'medium',
          src: imgUrl,
        }),
    },
    {
      key: 'maintainType',
      title: '保养成色类型',
      isTable: false,
      isForm: true,
      minWidth: 120,
    },
    {
      key: 'minCount',
      title: '最小参与人数',
      isTable: false,
      isForm: true,
    },
    {
      key: 'prdLocation',
      title: '产地',
      isTable: false,
      isForm: true,
    },
    {
      key: 'spec',
      title: '规格',
      isTable: false,
      isForm: true,
      minWidth: 100,
    },
    {
      key: 'purity',
      title: '纯度',
      isTable: false,
      isForm: true,
    },
    {
      key: 'registrationFee',
      title: '报名费',
      isTable: false,
      isForm: true,
      minWidth: 100,
    },
    {
      key: 'whAddress',
      title: '仓库地址',
      isTable: false,
      isForm: true,
      minWidth: 120,
    },
    {
      key: 'reservePrice',
      title: '是否保留价格',
      isTable: false,
      isForm: true,
      type: 'bool',
      minWidth: 70,
    },
    {
      key: 'status',
      title: '审核状态',
      isTable: true,
      isForm: true,
      isSetRole: true,
      minWidth: 100,
      options: statusOptions,
      render(row) {
        const option = statusOptions.find((i) => i.id == row.status) || {}
        return [
          h(
            NTag,
            { type: option.type },
            {
              default: () => option.name || '-',
            }
          ),
        ]
      },
    },
    {
      key: 'serviceFeeRatio',
      title: '服务费比例',
      isTable: false,
      isForm: true,
      minWidth: 100,
    },
    {
      key: 'startingBid',
      title: '起拍价',
      isTable: false,
      isForm: true,
      minWidth: 100,
    },
    {
      key: 'taxRate',
      title: '税率',
      isTable: false,
      isForm: true,
    },
    {
      key: 'title',
      title: '标题',
      isTable: false,
      isForm: false,
    },
    {
      key: 'tradeInfo',
      title: '交易信息',
      isTable: false,
      isForm: false,
    },
    {
      key: 'tradePrice',
      title: '交易价格',
      isTable: false,
      isForm: false,
      minWidth: 100,
    },
    {
      key: 'effectiveTime',
      title: '有效时间',
      isTable: false,
      isForm: true,
      minWidth: 120,
      type: 'datetime',
    },
    {
      key: 'createTime',
      title: '创建时间',
      isTable: false,
      isForm: false,
      minWidth: 180,
    },
    {
      key: 'updateTime',
      title: '更新时间',
      isTable: false,
      isForm: false,
      minWidth: 180,
    },
    {
      key: 'auctionStartTime',
      title: '竞拍开始时间',
      isTable: false,
      isForm: true,
      type: 'datetime',
      minWidth: 180,
    },
    {
      key: 'auctionEndTime',
      title: '竞拍结束时间',
      isTable: false,
      isForm: true,
      type: 'datetime',
      minWidth: 180,
    },
    {
      key: 'div',
      title: '',
      isForm: true,
      isFirst: true,
      span: 24,
      spanText: '交易信息'
    },
    {
      key: 'goodsUserRelate',
      title: '授权买家',
      isForm: true,
      options: [],
      multiple: true,
      isRemoteOptions: true,
      // options: statusOptions,
    },
    {
      key: 'payType',
      title: '付款方式',
      isForm: true,
      minWidth: 100,
    },
    {
      key: 'payTerm',
      title: '交付条款',
      isForm: true,
    },
    {
      key: 'deliveryType',
      title: '交割方式',
      isForm: true,
    },
    {
      key: 'deliveryCondition',
      title: '装运条件',
      isForm: true,
    },
    {
      key: 'deliveryTime',
      title: '交货时间',
      isForm: true,
      type: 'datetime',
    },
    {
      key: 'deliveryErrand',
      title: '交货公差',
      isForm: true,
    },
    {
      key: 'contractUrl',
      title: '合同链接',
      isForm: true,
    },
    {
      title: '操作',
      key: 'actions',
      width: 220,
      align: 'right',
      fixed: 'right',
      hideInExcel: true,
      isTable: true,
      render(row) {
        return [
          h(
            NButton,
            {
              size: 'small',
              type: 'primary',
              secondary: true,
              onClick: () => ctx.handleOpenRolesSet(row),
            },
            {
              default: () => '设置审核状态',
            }
          ),

          h(
            NButton,
            {
              size: 'small',
              type: 'primary',
              style: 'margin-left: 12px;',
              onClick: () => {
                ctx.handleOpen({
                  action: 'setGoods',
                  title: '修改商品',
                  row: {
                    ...row,
                    goodsUserRelate: row.goodsUserRelate?.split(',').map(Number).filter(Boolean) || [],
                    effectiveTime: dayjs(row.effectiveTime || dayjs()).valueOf(),
                    deliveryTime: dayjs(row.deliveryTime || dayjs()).valueOf(),
                    auctionStartTime: dayjs(row.auctionStartTime || dayjs()).valueOf(),
                    auctionEndTime: dayjs(row.auctionEndTime || dayjs()).valueOf(),
                  },
                  onOk: ctx.onSave,
                })
              },
            },
            {
              default: () => '修改商品',
            }
          ),

          // h(
          //   NButton,
          //   {
          //     size: 'small',
          //     type: 'error',
          //     style: 'margin-left: 12px;',
          //     onClick: () => ctx.handleDelete(row.id),
          //   },
          //   {
          //     default: () => '删除',
          //   }
          // ),
        ]
      },
    },
  ]
}
