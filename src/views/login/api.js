import { request } from '@/utils'

export default {
  toggleRole: (data) => request.post('/auth/role/toggle', data),
  login: (data) => request.post('/login', data, { noNeedToken: true }),
  getUser: (data) => request.post('/user/detail', data),
}
