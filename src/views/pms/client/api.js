import { useUserStore } from '@/store'
import { request } from '@/utils'
import { NTag } from 'naive-ui'
import { NButton, NAvatar } from 'naive-ui'

export default {
  create: (data) => request.post('/user/register', { ...data, }),
  read: (params = {}) => request.post('/user/list', { ...params, userType: 0, }),
  update: (data) => request.post(`/user/update`, { ...data, }),
  delete: (id) => request.post(`/user/delete`, { id }),
  resetPwd: (id, data) => request.post(`/user/update`, { ...data, }),

  getAllRoles: () => request.get('/role?enable=1'),
}

// 'primary' | 'info' | 'success' | 'warning' | 'error'
export const statusOptions = [
  { id: 0, name: '待审核', type: 'info' },
  { id: 1, name: '审核成功', type: 'success' },
  { id: 2, name: '审核失败', type: 'error' },
]

export const typeOptions = [
  { id: 0, name: '买家', type: 'primary' },
  { id: 1, name: '供应商', type: 'info' },
  { id: 2, name: '管理员', type: 'success' },
]

export const genColumns = (ctx) => {
  const { userInfo } = useUserStore()
  return [
    {
      key: 'id',
      title: 'ID',
      isTable: true,
      isForm: false,
      width: 50,
    },
    {
      key: 'imgUrl',
      title: '头像',
      isTable: true,
      isForm: true,
      isImage: true,
      minWidth: 70,
      render: ({ imgUrl }) =>
        ctx.h(NAvatar, {
          size: 'medium',
          src: imgUrl,
        }),
    },
    {
      key: 'userName',
      title: '用户名',
      isTable: true,
      isForm: true,
      minWidth: 150,
    },
    {
      key: 'nickName',
      title: '昵称',
      isTable: true,
      isForm: true,
      minWidth: 120,
    },
    {
      key: 'password',
      title: '密码',
      isTable: false,
      isForm: true,
      isResetPwd: true,
    },
    {
      key: 'contact',
      title: '联系人',
      isTable: true,
      isForm: true,
      minWidth: 100,
    },
    {
      key: 'email',
      title: '邮箱',
      isTable: true,
      isForm: true,
      minWidth: 170,
    },
    {
      key: 'mobile',
      title: '手机号',
      isTable: true,
      isForm: true,
      minWidth: 120,
    },
    {
      key: 'contactMobile',
      title: '联系手机号',
      isTable: true,
      isForm: true,
      minWidth: 130,
    },
    {
      key: 'userType',
      title: '用户类型',
      isTable: true,
      isForm: true,
      minWidth: 120,
      options: typeOptions,
      render(row) {
        const h = ctx.h
        const option = typeOptions.find((i) => i.id == row.userType)
        return [
          h(
            NTag,
            { type: option.type },
            {
              default: () => option.name,
            }
          ),
        ]
      },
    },
    {
      key: 'status',
      title: '审核状态',
      desc: '0:待审核, 1：审核成功, 2:审核失败',
      isTable: true,
      isForm: true,
      isSetRole: true,
      options: statusOptions,
      render(row) {
        const h = ctx.h
        const option = statusOptions.find((i) => i.id == row.status)
        return [
          h(
            NTag,
            { type: option.type },
            {
              default: () => option.name,
            }
          ),
        ]
      },
    },
    // {
    //   key: 'source',
    //   title: '来源',
    //   isTable: true,
    //   isForm: false,
    //   minWidth: 80,
    //   render({ source }) {
    //     return [ctx.h(NTag, { type: 'info' }, { default: () => (source ? 'PC' : '小程序') })]
    //   },
    // },
    {
      key: 'licenseUrl',
      title: '营业执照',
      isTable: false,
      isForm: true,
      isImage: true,
    },
    {
      key: 'creditCode',
      title: '统一社会信用代码',
      isTable: false,
      isForm: true,
      minWidth: 140,
    },
    {
      key: 'platformSupplier',
      title: '申请平台供应商',
      isTable: false,
      isForm: true,
    },
    {
      key: 'platformSupplierCode',
      title: '平台供应商编码',
      isTable: false,
      isForm: true,
    },
    {
      key: 'createTime',
      title: '创建时间',
      isTable: false,
      isForm: false,
    },
    {
      key: 'registerTime',
      title: '注册时间',
      isTable: true,
      isForm: false,
      minWidth: 180,
    },
    {
      key: 'updateTime',
      title: '更新时间',
      isTable: false,
      isForm: false,
    },
    {
      key: 'ext',
      title: '拓展字段',
      isTable: false,
      isForm: true,
    },
    {
      key: 'externalUserId',
      title: '第三方用户ID',
      isTable: false,
      isForm: true,
    },
    {
      title: '操作',
      key: 'actions',
      minWidth: 100,
      align: 'right',
      fixed: 'right',
      hideInExcel: true,
      isTable: true,
      render(row) {
        const h = ctx.h
        return [
          h(
            NButton,
            {
              size: 'small',
              type: 'primary',
              secondary: true,
              onClick: () => ctx.handleOpenRolesSet(row),
            },
            {
              default: () => '设置审核状态',
            }
          ),
          h(
            NButton,
            {
              size: 'small',
              type: 'primary',
              secondary: true,
              style: 'margin-left: 12px;',
              onClick: () => {
                ctx.handleOpen({ action: 'view', title: '详情', row, showOk: false })
              },
            },
            {
              default: () => '详情',
            }
          ),
          h(
            NButton,
            {
              size: 'small',
              type: 'primary',
              style: 'margin-left: 12px;',
              onClick: () =>
                ctx.handleOpen({ action: 'reset', title: '重置密码', row, onOk: ctx.onSave }),
            },
            {
              default: () => '重置密码',
            }
          ),

          // h(
          //   NButton,
          //   {
          //     size: 'small',
          //     type: 'error',
          //     style: 'margin-left: 12px;',
          //     onClick: () => ctx.handleDelete(row.id),
          //   },
          //   {
          //     default: () => '删除',
          //     // icon: () => h('i', { class: 'i-material-symbols:delete-outline text-14' }),
          //   }
          // ),
        ]
      },
    },
  ]
}
