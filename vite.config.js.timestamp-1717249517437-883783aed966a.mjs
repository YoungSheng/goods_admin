// vite.config.js
import path2 from "path";
import { defineConfig, loadEnv } from "file:///root/project/goods_admin/node_modules/.pnpm/vite@5.2.11_sass@1.77.1/node_modules/vite/dist/node/index.js";
import Vue from "file:///root/project/goods_admin/node_modules/.pnpm/@vitejs+plugin-vue@5.0.4_vite@5.2.11_vue@3.4.27/node_modules/@vitejs/plugin-vue/dist/index.mjs";
import VueDevTools from "file:///root/project/goods_admin/node_modules/.pnpm/vite-plugin-vue-devtools@7.1.3_vite@5.2.11_vue@3.4.27/node_modules/vite-plugin-vue-devtools/dist/vite.mjs";
import Unocss from "file:///root/project/goods_admin/node_modules/.pnpm/unocss@0.59.4_postcss@8.4.38_vite@5.2.11/node_modules/unocss/dist/vite.mjs";
import AutoImport from "file:///root/project/goods_admin/node_modules/.pnpm/unplugin-auto-import@0.17.6_@vueuse+core@10.9.0/node_modules/unplugin-auto-import/dist/vite.js";
import Components from "file:///root/project/goods_admin/node_modules/.pnpm/unplugin-vue-components@0.26.0_vue@3.4.27/node_modules/unplugin-vue-components/dist/vite.js";
import { NaiveUiResolver } from "file:///root/project/goods_admin/node_modules/.pnpm/unplugin-vue-components@0.26.0_vue@3.4.27/node_modules/unplugin-vue-components/dist/resolvers.js";
import simpleHtmlPlugin from "file:///root/project/goods_admin/node_modules/.pnpm/vite-plugin-simple-html@0.1.2_vite@5.2.11/node_modules/vite-plugin-simple-html/dist/index.js";
import removeNoMatch from "file:///root/project/goods_admin/node_modules/.pnpm/vite-plugin-router-warn@1.0.0/node_modules/vite-plugin-router-warn/dist/index.mjs";

// build/index.js
import { globSync } from "file:///root/project/goods_admin/node_modules/.pnpm/glob@10.3.15/node_modules/glob/dist/esm/index.js";
import path from "path";

// src/assets/icons/dynamic-icons.js
var dynamic_icons_default = ["i-simple-icons:juejin"];

// build/index.js
function getIcons() {
  const feFiles = globSync("src/assets/icons/feather/*.svg", { nodir: true, strict: true });
  const meFiles = globSync("src/assets/icons/isme/*.svg", { nodir: true, strict: true });
  const feIcons = feFiles.map((filePath) => {
    const fileName = path.basename(filePath);
    const fileNameWithoutExt = path.parse(fileName).name;
    return `i-fe:${fileNameWithoutExt}`;
  });
  const meIcons = meFiles.map((filePath) => {
    const fileName = path.basename(filePath);
    const fileNameWithoutExt = path.parse(fileName).name;
    return `i-me:${fileNameWithoutExt}`;
  });
  return [...dynamic_icons_default, ...feIcons, ...meIcons];
}
function getPagePathes() {
  const files = globSync("src/views/**/*.vue");
  return files.map((item) => "/" + path.normalize(item).replace(/\\/g, "/"));
}

// build/plugin-isme/page-pathes.js
var PLUGIN_PAGE_PATHES_ID = "isme:page-pathes";
function pluginPagePathes() {
  return {
    name: "isme:page-pathes",
    resolveId(id) {
      if (id === PLUGIN_PAGE_PATHES_ID)
        return "\0" + PLUGIN_PAGE_PATHES_ID;
    },
    load(id) {
      if (id === "\0" + PLUGIN_PAGE_PATHES_ID) {
        return `export default ${JSON.stringify(getPagePathes())}`;
      }
    }
  };
}

// build/plugin-isme/icons.js
var PLUGIN_ICONS_ID = "isme:icons";
function pluginIcons() {
  return {
    name: "isme:icons",
    resolveId(id) {
      if (id === PLUGIN_ICONS_ID)
        return "\0" + PLUGIN_ICONS_ID;
    },
    load(id) {
      if (id === "\0" + PLUGIN_ICONS_ID) {
        return `export default ${JSON.stringify(getIcons())}`;
      }
    }
  };
}

// vite.config.js
var vite_config_default = defineConfig(({ command, mode }) => {
  const isBuild = command === "build";
  const viteEnv = loadEnv(mode, process.cwd());
  const { VITE_TITLE, VITE_PUBLIC_PATH, VITE_PROXY_TARGET } = viteEnv;
  return {
    base: VITE_PUBLIC_PATH || "/",
    plugins: [
      Vue(),
      VueDevTools(),
      Unocss(),
      AutoImport({
        imports: ["vue", "vue-router"],
        dts: false
      }),
      Components({
        resolvers: [NaiveUiResolver()],
        dts: false
      }),
      simpleHtmlPlugin({
        minify: isBuild,
        inject: {
          data: {
            title: VITE_TITLE
          }
        }
      }),
      // 自定义插件，用于生成页面文件的path，并添加到虚拟模块
      pluginPagePathes(),
      // 自定义插件，用于生成自定义icon，并添加到虚拟模块
      pluginIcons(),
      // 移除非必要的vue-router动态路由警告: No match found for location with path
      removeNoMatch()
    ],
    resolve: {
      alias: {
        "@": path2.resolve(process.cwd(), "src"),
        "~": path2.resolve(process.cwd())
      }
    },
    server: {
      host: "0.0.0.0",
      port: 3200,
      open: false,
      proxy: {
        "/api": {
          target: VITE_PROXY_TARGET,
          changeOrigin: true,
          rewrite: (path3) => path3.replace(new RegExp("^/api"), ""),
          secure: false,
          configure: (proxy, options) => {
            proxy.on("proxyRes", (proxyRes, req) => {
              proxyRes.headers["x-real-url"] = new URL(req.url || "", options.target)?.href || "";
            });
          }
        }
      }
    },
    build: {
      chunkSizeWarningLimit: 1024
      // chunk 大小警告的限制（单位kb）
    }
  };
});
export {
  vite_config_default as default
};
//# sourceMappingURL=data:application/json;base64,ewogICJ2ZXJzaW9uIjogMywKICAic291cmNlcyI6IFsidml0ZS5jb25maWcuanMiLCAiYnVpbGQvaW5kZXguanMiLCAic3JjL2Fzc2V0cy9pY29ucy9keW5hbWljLWljb25zLmpzIiwgImJ1aWxkL3BsdWdpbi1pc21lL3BhZ2UtcGF0aGVzLmpzIiwgImJ1aWxkL3BsdWdpbi1pc21lL2ljb25zLmpzIl0sCiAgInNvdXJjZXNDb250ZW50IjogWyJjb25zdCBfX3ZpdGVfaW5qZWN0ZWRfb3JpZ2luYWxfZGlybmFtZSA9IFwiL3Jvb3QvcHJvamVjdC9nb29kc19hZG1pblwiO2NvbnN0IF9fdml0ZV9pbmplY3RlZF9vcmlnaW5hbF9maWxlbmFtZSA9IFwiL3Jvb3QvcHJvamVjdC9nb29kc19hZG1pbi92aXRlLmNvbmZpZy5qc1wiO2NvbnN0IF9fdml0ZV9pbmplY3RlZF9vcmlnaW5hbF9pbXBvcnRfbWV0YV91cmwgPSBcImZpbGU6Ly8vcm9vdC9wcm9qZWN0L2dvb2RzX2FkbWluL3ZpdGUuY29uZmlnLmpzXCI7aW1wb3J0IHBhdGggZnJvbSAncGF0aCdcbmltcG9ydCB7IGRlZmluZUNvbmZpZywgbG9hZEVudiB9IGZyb20gJ3ZpdGUnXG5pbXBvcnQgVnVlIGZyb20gJ0B2aXRlanMvcGx1Z2luLXZ1ZSdcbmltcG9ydCBWdWVEZXZUb29scyBmcm9tICd2aXRlLXBsdWdpbi12dWUtZGV2dG9vbHMnXG5pbXBvcnQgVW5vY3NzIGZyb20gJ3Vub2Nzcy92aXRlJ1xuaW1wb3J0IEF1dG9JbXBvcnQgZnJvbSAndW5wbHVnaW4tYXV0by1pbXBvcnQvdml0ZSdcbmltcG9ydCBDb21wb25lbnRzIGZyb20gJ3VucGx1Z2luLXZ1ZS1jb21wb25lbnRzL3ZpdGUnXG5pbXBvcnQgeyBOYWl2ZVVpUmVzb2x2ZXIgfSBmcm9tICd1bnBsdWdpbi12dWUtY29tcG9uZW50cy9yZXNvbHZlcnMnXG5pbXBvcnQgc2ltcGxlSHRtbFBsdWdpbiBmcm9tICd2aXRlLXBsdWdpbi1zaW1wbGUtaHRtbCdcbmltcG9ydCByZW1vdmVOb01hdGNoIGZyb20gJ3ZpdGUtcGx1Z2luLXJvdXRlci13YXJuJ1xuaW1wb3J0IHsgcGx1Z2luUGFnZVBhdGhlcywgcGx1Z2luSWNvbnMgfSBmcm9tICcuL2J1aWxkL3BsdWdpbi1pc21lJ1xuXG5leHBvcnQgZGVmYXVsdCBkZWZpbmVDb25maWcoKHsgY29tbWFuZCwgbW9kZSB9KSA9PiB7XG4gIGNvbnN0IGlzQnVpbGQgPSBjb21tYW5kID09PSAnYnVpbGQnXG4gIGNvbnN0IHZpdGVFbnYgPSBsb2FkRW52KG1vZGUsIHByb2Nlc3MuY3dkKCkpXG4gIGNvbnN0IHsgVklURV9USVRMRSwgVklURV9QVUJMSUNfUEFUSCwgVklURV9QUk9YWV9UQVJHRVQgfSA9IHZpdGVFbnZcblxuICByZXR1cm4ge1xuICAgIGJhc2U6IFZJVEVfUFVCTElDX1BBVEggfHwgJy8nLFxuICAgIHBsdWdpbnM6IFtcbiAgICAgIFZ1ZSgpLFxuICAgICAgVnVlRGV2VG9vbHMoKSxcbiAgICAgIFVub2NzcygpLFxuICAgICAgQXV0b0ltcG9ydCh7XG4gICAgICAgIGltcG9ydHM6IFsndnVlJywgJ3Z1ZS1yb3V0ZXInXSxcbiAgICAgICAgZHRzOiBmYWxzZSxcbiAgICAgIH0pLFxuICAgICAgQ29tcG9uZW50cyh7XG4gICAgICAgIHJlc29sdmVyczogW05haXZlVWlSZXNvbHZlcigpXSxcbiAgICAgICAgZHRzOiBmYWxzZSxcbiAgICAgIH0pLFxuICAgICAgc2ltcGxlSHRtbFBsdWdpbih7XG4gICAgICAgIG1pbmlmeTogaXNCdWlsZCxcbiAgICAgICAgaW5qZWN0OiB7XG4gICAgICAgICAgZGF0YToge1xuICAgICAgICAgICAgdGl0bGU6IFZJVEVfVElUTEUsXG4gICAgICAgICAgfSxcbiAgICAgICAgfSxcbiAgICAgIH0pLFxuICAgICAgLy8gXHU4MUVBXHU1QjlBXHU0RTQ5XHU2M0QyXHU0RUY2XHVGRjBDXHU3NTI4XHU0RThFXHU3NTFGXHU2MjEwXHU5ODc1XHU5NzYyXHU2NTg3XHU0RUY2XHU3Njg0cGF0aFx1RkYwQ1x1NUU3Nlx1NkRGQlx1NTJBMFx1NTIzMFx1ODY1QVx1NjJERlx1NkEyMVx1NTc1N1xuICAgICAgcGx1Z2luUGFnZVBhdGhlcygpLFxuICAgICAgLy8gXHU4MUVBXHU1QjlBXHU0RTQ5XHU2M0QyXHU0RUY2XHVGRjBDXHU3NTI4XHU0RThFXHU3NTFGXHU2MjEwXHU4MUVBXHU1QjlBXHU0RTQ5aWNvblx1RkYwQ1x1NUU3Nlx1NkRGQlx1NTJBMFx1NTIzMFx1ODY1QVx1NjJERlx1NkEyMVx1NTc1N1xuICAgICAgcGx1Z2luSWNvbnMoKSxcbiAgICAgIC8vIFx1NzlGQlx1OTY2NFx1OTc1RVx1NUZDNVx1ODk4MVx1NzY4NHZ1ZS1yb3V0ZXJcdTUyQThcdTYwMDFcdThERUZcdTc1MzFcdThCNjZcdTU0NEE6IE5vIG1hdGNoIGZvdW5kIGZvciBsb2NhdGlvbiB3aXRoIHBhdGhcbiAgICAgIHJlbW92ZU5vTWF0Y2goKSxcbiAgICBdLFxuICAgIHJlc29sdmU6IHtcbiAgICAgIGFsaWFzOiB7XG4gICAgICAgICdAJzogcGF0aC5yZXNvbHZlKHByb2Nlc3MuY3dkKCksICdzcmMnKSxcbiAgICAgICAgJ34nOiBwYXRoLnJlc29sdmUocHJvY2Vzcy5jd2QoKSksXG4gICAgICB9LFxuICAgIH0sXG4gICAgc2VydmVyOiB7XG4gICAgICBob3N0OiAnMC4wLjAuMCcsXG4gICAgICBwb3J0OiAzMjAwLFxuICAgICAgb3BlbjogZmFsc2UsXG4gICAgICBwcm94eToge1xuICAgICAgICAnL2FwaSc6IHtcbiAgICAgICAgICB0YXJnZXQ6IFZJVEVfUFJPWFlfVEFSR0VULFxuICAgICAgICAgIGNoYW5nZU9yaWdpbjogdHJ1ZSxcbiAgICAgICAgICByZXdyaXRlOiAocGF0aCkgPT4gcGF0aC5yZXBsYWNlKG5ldyBSZWdFeHAoJ14vYXBpJyksICcnKSxcbiAgICAgICAgICBzZWN1cmU6IGZhbHNlLFxuICAgICAgICAgIGNvbmZpZ3VyZTogKHByb3h5LCBvcHRpb25zKSA9PiB7XG4gICAgICAgICAgICAvLyBcdTkxNERcdTdGNkVcdTZCNjRcdTk4NzlcdTUzRUZcdTU3MjhcdTU0Q0RcdTVFOTRcdTU5MzRcdTRFMkRcdTc3MEJcdTUyMzBcdThCRjdcdTZDNDJcdTc2ODRcdTc3MUZcdTVCOUVcdTU3MzBcdTU3NDBcbiAgICAgICAgICAgIHByb3h5Lm9uKCdwcm94eVJlcycsIChwcm94eVJlcywgcmVxKSA9PiB7XG4gICAgICAgICAgICAgIHByb3h5UmVzLmhlYWRlcnNbJ3gtcmVhbC11cmwnXSA9IG5ldyBVUkwocmVxLnVybCB8fCAnJywgb3B0aW9ucy50YXJnZXQpPy5ocmVmIHx8ICcnXG4gICAgICAgICAgICB9KVxuICAgICAgICAgIH0sXG4gICAgICAgIH0sXG4gICAgICB9LFxuICAgIH0sXG4gICAgYnVpbGQ6IHtcbiAgICAgIGNodW5rU2l6ZVdhcm5pbmdMaW1pdDogMTAyNCwgLy8gY2h1bmsgXHU1OTI3XHU1QzBGXHU4QjY2XHU1NDRBXHU3Njg0XHU5NjUwXHU1MjM2XHVGRjA4XHU1MzU1XHU0RjREa2JcdUZGMDlcbiAgICB9LFxuICB9XG59KVxuIiwgImNvbnN0IF9fdml0ZV9pbmplY3RlZF9vcmlnaW5hbF9kaXJuYW1lID0gXCIvcm9vdC9wcm9qZWN0L2dvb2RzX2FkbWluL2J1aWxkXCI7Y29uc3QgX192aXRlX2luamVjdGVkX29yaWdpbmFsX2ZpbGVuYW1lID0gXCIvcm9vdC9wcm9qZWN0L2dvb2RzX2FkbWluL2J1aWxkL2luZGV4LmpzXCI7Y29uc3QgX192aXRlX2luamVjdGVkX29yaWdpbmFsX2ltcG9ydF9tZXRhX3VybCA9IFwiZmlsZTovLy9yb290L3Byb2plY3QvZ29vZHNfYWRtaW4vYnVpbGQvaW5kZXguanNcIjtpbXBvcnQgeyBnbG9iU3luYyB9IGZyb20gJ2dsb2InXG5pbXBvcnQgcGF0aCBmcm9tICdwYXRoJ1xuaW1wb3J0IGR5bmFtaWNJY29ucyBmcm9tICcuLi9zcmMvYXNzZXRzL2ljb25zL2R5bmFtaWMtaWNvbnMnXG5cbi8qKlxuICogQHVzYWdlIFx1NzUxRlx1NjIxMGljb25zLCBcdTc1MjhcdTRFOEUgdW5vY3NzIHNhZmVsaXN0XHVGRjBDXHU0RUU1XHU2NTJGXHU2MzAxXHU5ODc1XHU5NzYyXHU1MkE4XHU2MDAxXHU2RTMyXHU2N0QzXHU4MUVBXHU1QjlBXHU0RTQ5XHU1NkZFXHU2ODA3XG4gKi9cbmV4cG9ydCBmdW5jdGlvbiBnZXRJY29ucygpIHtcbiAgY29uc3QgZmVGaWxlcyA9IGdsb2JTeW5jKCdzcmMvYXNzZXRzL2ljb25zL2ZlYXRoZXIvKi5zdmcnLCB7IG5vZGlyOiB0cnVlLCBzdHJpY3Q6IHRydWUgfSlcbiAgY29uc3QgbWVGaWxlcyA9IGdsb2JTeW5jKCdzcmMvYXNzZXRzL2ljb25zL2lzbWUvKi5zdmcnLCB7IG5vZGlyOiB0cnVlLCBzdHJpY3Q6IHRydWUgfSlcbiAgY29uc3QgZmVJY29ucyA9IGZlRmlsZXMubWFwKChmaWxlUGF0aCkgPT4ge1xuICAgIGNvbnN0IGZpbGVOYW1lID0gcGF0aC5iYXNlbmFtZShmaWxlUGF0aCkgLy8gXHU4M0I3XHU1M0Q2XHU2NTg3XHU0RUY2XHU1NDBEXHVGRjBDXHU1MzA1XHU2MkVDXHU1NDBFXHU3RjAwXG4gICAgY29uc3QgZmlsZU5hbWVXaXRob3V0RXh0ID0gcGF0aC5wYXJzZShmaWxlTmFtZSkubmFtZSAvLyBcdTgzQjdcdTUzRDZcdTUzQkJcdTk2NjRcdTU0MEVcdTdGMDBcdTc2ODRcdTY1ODdcdTRFRjZcdTU0MERcbiAgICByZXR1cm4gYGktZmU6JHtmaWxlTmFtZVdpdGhvdXRFeHR9YFxuICB9KVxuICBjb25zdCBtZUljb25zID0gbWVGaWxlcy5tYXAoKGZpbGVQYXRoKSA9PiB7XG4gICAgY29uc3QgZmlsZU5hbWUgPSBwYXRoLmJhc2VuYW1lKGZpbGVQYXRoKSAvLyBcdTgzQjdcdTUzRDZcdTY1ODdcdTRFRjZcdTU0MERcdUZGMENcdTUzMDVcdTYyRUNcdTU0MEVcdTdGMDBcbiAgICBjb25zdCBmaWxlTmFtZVdpdGhvdXRFeHQgPSBwYXRoLnBhcnNlKGZpbGVOYW1lKS5uYW1lIC8vIFx1ODNCN1x1NTNENlx1NTNCQlx1OTY2NFx1NTQwRVx1N0YwMFx1NzY4NFx1NjU4N1x1NEVGNlx1NTQwRFxuICAgIHJldHVybiBgaS1tZToke2ZpbGVOYW1lV2l0aG91dEV4dH1gXG4gIH0pXG5cbiAgcmV0dXJuIFsuLi5keW5hbWljSWNvbnMsIC4uLmZlSWNvbnMsIC4uLm1lSWNvbnNdXG59XG5cbi8qKlxuICogQHVzYWdlIFx1NzUxRlx1NjIxMC52dWVcdTY1ODdcdTRFRjZcdThERUZcdTVGODRcdTUyMTdcdTg4NjhcdUZGMENcdTc1MjhcdTRFOEVcdTZERkJcdTUyQTBcdTgzRENcdTUzNTVcdTY1RjZcdTUzRUZcdTRFMEJcdTYyQzlcdTkwMDlcdTYyRTlcdTVCRjlcdTVFOTRcdTc2ODQudnVlXHU2NTg3XHU0RUY2XHU4REVGXHU1Rjg0XHVGRjBDXHU5NjMyXHU2QjYyXHU2MjRCXHU1MkE4XHU4RjkzXHU1MTY1XHU2MkE1XHU5NTE5XG4gKi9cbmV4cG9ydCBmdW5jdGlvbiBnZXRQYWdlUGF0aGVzKCkge1xuICBjb25zdCBmaWxlcyA9IGdsb2JTeW5jKCdzcmMvdmlld3MvKiovKi52dWUnKVxuICByZXR1cm4gZmlsZXMubWFwKChpdGVtKSA9PiAnLycgKyBwYXRoLm5vcm1hbGl6ZShpdGVtKS5yZXBsYWNlKC9cXFxcL2csICcvJykpXG59XG4iLCAiY29uc3QgX192aXRlX2luamVjdGVkX29yaWdpbmFsX2Rpcm5hbWUgPSBcIi9yb290L3Byb2plY3QvZ29vZHNfYWRtaW4vc3JjL2Fzc2V0cy9pY29uc1wiO2NvbnN0IF9fdml0ZV9pbmplY3RlZF9vcmlnaW5hbF9maWxlbmFtZSA9IFwiL3Jvb3QvcHJvamVjdC9nb29kc19hZG1pbi9zcmMvYXNzZXRzL2ljb25zL2R5bmFtaWMtaWNvbnMuanNcIjtjb25zdCBfX3ZpdGVfaW5qZWN0ZWRfb3JpZ2luYWxfaW1wb3J0X21ldGFfdXJsID0gXCJmaWxlOi8vL3Jvb3QvcHJvamVjdC9nb29kc19hZG1pbi9zcmMvYXNzZXRzL2ljb25zL2R5bmFtaWMtaWNvbnMuanNcIjsvLyBcdTk3MDBcdTg5ODFcdTUyQThcdTYwMDFcdTZFMzJcdTY3RDNcdTc2ODRpY29uaWZ5XHU1NkZFXHU2ODA3XHVGRjBDXHU0RUU1aS1cdTVGMDBcdTU5MzRcbmV4cG9ydCBkZWZhdWx0IFsnaS1zaW1wbGUtaWNvbnM6anVlamluJ10iLCAiY29uc3QgX192aXRlX2luamVjdGVkX29yaWdpbmFsX2Rpcm5hbWUgPSBcIi9yb290L3Byb2plY3QvZ29vZHNfYWRtaW4vYnVpbGQvcGx1Z2luLWlzbWVcIjtjb25zdCBfX3ZpdGVfaW5qZWN0ZWRfb3JpZ2luYWxfZmlsZW5hbWUgPSBcIi9yb290L3Byb2plY3QvZ29vZHNfYWRtaW4vYnVpbGQvcGx1Z2luLWlzbWUvcGFnZS1wYXRoZXMuanNcIjtjb25zdCBfX3ZpdGVfaW5qZWN0ZWRfb3JpZ2luYWxfaW1wb3J0X21ldGFfdXJsID0gXCJmaWxlOi8vL3Jvb3QvcHJvamVjdC9nb29kc19hZG1pbi9idWlsZC9wbHVnaW4taXNtZS9wYWdlLXBhdGhlcy5qc1wiO2ltcG9ydCB7IGdldFBhZ2VQYXRoZXMgfSBmcm9tICcuLidcblxuY29uc3QgUExVR0lOX1BBR0VfUEFUSEVTX0lEID0gJ2lzbWU6cGFnZS1wYXRoZXMnXG5leHBvcnQgZnVuY3Rpb24gcGx1Z2luUGFnZVBhdGhlcygpIHtcbiAgcmV0dXJuIHtcbiAgICBuYW1lOiAnaXNtZTpwYWdlLXBhdGhlcycsXG4gICAgcmVzb2x2ZUlkKGlkKSB7XG4gICAgICBpZiAoaWQgPT09IFBMVUdJTl9QQUdFX1BBVEhFU19JRCkgcmV0dXJuICdcXDAnICsgUExVR0lOX1BBR0VfUEFUSEVTX0lEXG4gICAgfSxcbiAgICBsb2FkKGlkKSB7XG4gICAgICBpZiAoaWQgPT09ICdcXDAnICsgUExVR0lOX1BBR0VfUEFUSEVTX0lEKSB7XG4gICAgICAgIHJldHVybiBgZXhwb3J0IGRlZmF1bHQgJHtKU09OLnN0cmluZ2lmeShnZXRQYWdlUGF0aGVzKCkpfWBcbiAgICAgIH1cbiAgICB9LFxuICB9XG59XG4iLCAiY29uc3QgX192aXRlX2luamVjdGVkX29yaWdpbmFsX2Rpcm5hbWUgPSBcIi9yb290L3Byb2plY3QvZ29vZHNfYWRtaW4vYnVpbGQvcGx1Z2luLWlzbWVcIjtjb25zdCBfX3ZpdGVfaW5qZWN0ZWRfb3JpZ2luYWxfZmlsZW5hbWUgPSBcIi9yb290L3Byb2plY3QvZ29vZHNfYWRtaW4vYnVpbGQvcGx1Z2luLWlzbWUvaWNvbnMuanNcIjtjb25zdCBfX3ZpdGVfaW5qZWN0ZWRfb3JpZ2luYWxfaW1wb3J0X21ldGFfdXJsID0gXCJmaWxlOi8vL3Jvb3QvcHJvamVjdC9nb29kc19hZG1pbi9idWlsZC9wbHVnaW4taXNtZS9pY29ucy5qc1wiO2ltcG9ydCB7IGdldEljb25zIH0gZnJvbSAnLi4nXG5cbmNvbnN0IFBMVUdJTl9JQ09OU19JRCA9ICdpc21lOmljb25zJ1xuZXhwb3J0IGZ1bmN0aW9uIHBsdWdpbkljb25zKCkge1xuICByZXR1cm4ge1xuICAgIG5hbWU6ICdpc21lOmljb25zJyxcbiAgICByZXNvbHZlSWQoaWQpIHtcbiAgICAgIGlmIChpZCA9PT0gUExVR0lOX0lDT05TX0lEKSByZXR1cm4gJ1xcMCcgKyBQTFVHSU5fSUNPTlNfSURcbiAgICB9LFxuICAgIGxvYWQoaWQpIHtcbiAgICAgIGlmIChpZCA9PT0gJ1xcMCcgKyBQTFVHSU5fSUNPTlNfSUQpIHtcbiAgICAgICAgcmV0dXJuIGBleHBvcnQgZGVmYXVsdCAke0pTT04uc3RyaW5naWZ5KGdldEljb25zKCkpfWBcbiAgICAgIH1cbiAgICB9LFxuICB9XG59XG4iXSwKICAibWFwcGluZ3MiOiAiO0FBQTZQLE9BQU9BLFdBQVU7QUFDOVEsU0FBUyxjQUFjLGVBQWU7QUFDdEMsT0FBTyxTQUFTO0FBQ2hCLE9BQU8saUJBQWlCO0FBQ3hCLE9BQU8sWUFBWTtBQUNuQixPQUFPLGdCQUFnQjtBQUN2QixPQUFPLGdCQUFnQjtBQUN2QixTQUFTLHVCQUF1QjtBQUNoQyxPQUFPLHNCQUFzQjtBQUM3QixPQUFPLG1CQUFtQjs7O0FDVHlPLFNBQVMsZ0JBQWdCO0FBQzVSLE9BQU8sVUFBVTs7O0FDQWpCLElBQU8sd0JBQVEsQ0FBQyx1QkFBdUI7OztBRE1oQyxTQUFTLFdBQVc7QUFDekIsUUFBTSxVQUFVLFNBQVMsa0NBQWtDLEVBQUUsT0FBTyxNQUFNLFFBQVEsS0FBSyxDQUFDO0FBQ3hGLFFBQU0sVUFBVSxTQUFTLCtCQUErQixFQUFFLE9BQU8sTUFBTSxRQUFRLEtBQUssQ0FBQztBQUNyRixRQUFNLFVBQVUsUUFBUSxJQUFJLENBQUMsYUFBYTtBQUN4QyxVQUFNLFdBQVcsS0FBSyxTQUFTLFFBQVE7QUFDdkMsVUFBTSxxQkFBcUIsS0FBSyxNQUFNLFFBQVEsRUFBRTtBQUNoRCxXQUFPLFFBQVEsa0JBQWtCO0FBQUEsRUFDbkMsQ0FBQztBQUNELFFBQU0sVUFBVSxRQUFRLElBQUksQ0FBQyxhQUFhO0FBQ3hDLFVBQU0sV0FBVyxLQUFLLFNBQVMsUUFBUTtBQUN2QyxVQUFNLHFCQUFxQixLQUFLLE1BQU0sUUFBUSxFQUFFO0FBQ2hELFdBQU8sUUFBUSxrQkFBa0I7QUFBQSxFQUNuQyxDQUFDO0FBRUQsU0FBTyxDQUFDLEdBQUcsdUJBQWMsR0FBRyxTQUFTLEdBQUcsT0FBTztBQUNqRDtBQUtPLFNBQVMsZ0JBQWdCO0FBQzlCLFFBQU0sUUFBUSxTQUFTLG9CQUFvQjtBQUMzQyxTQUFPLE1BQU0sSUFBSSxDQUFDLFNBQVMsTUFBTSxLQUFLLFVBQVUsSUFBSSxFQUFFLFFBQVEsT0FBTyxHQUFHLENBQUM7QUFDM0U7OztBRTVCQSxJQUFNLHdCQUF3QjtBQUN2QixTQUFTLG1CQUFtQjtBQUNqQyxTQUFPO0FBQUEsSUFDTCxNQUFNO0FBQUEsSUFDTixVQUFVLElBQUk7QUFDWixVQUFJLE9BQU87QUFBdUIsZUFBTyxPQUFPO0FBQUEsSUFDbEQ7QUFBQSxJQUNBLEtBQUssSUFBSTtBQUNQLFVBQUksT0FBTyxPQUFPLHVCQUF1QjtBQUN2QyxlQUFPLGtCQUFrQixLQUFLLFVBQVUsY0FBYyxDQUFDLENBQUM7QUFBQSxNQUMxRDtBQUFBLElBQ0Y7QUFBQSxFQUNGO0FBQ0Y7OztBQ2JBLElBQU0sa0JBQWtCO0FBQ2pCLFNBQVMsY0FBYztBQUM1QixTQUFPO0FBQUEsSUFDTCxNQUFNO0FBQUEsSUFDTixVQUFVLElBQUk7QUFDWixVQUFJLE9BQU87QUFBaUIsZUFBTyxPQUFPO0FBQUEsSUFDNUM7QUFBQSxJQUNBLEtBQUssSUFBSTtBQUNQLFVBQUksT0FBTyxPQUFPLGlCQUFpQjtBQUNqQyxlQUFPLGtCQUFrQixLQUFLLFVBQVUsU0FBUyxDQUFDLENBQUM7QUFBQSxNQUNyRDtBQUFBLElBQ0Y7QUFBQSxFQUNGO0FBQ0Y7OztBSkhBLElBQU8sc0JBQVEsYUFBYSxDQUFDLEVBQUUsU0FBUyxLQUFLLE1BQU07QUFDakQsUUFBTSxVQUFVLFlBQVk7QUFDNUIsUUFBTSxVQUFVLFFBQVEsTUFBTSxRQUFRLElBQUksQ0FBQztBQUMzQyxRQUFNLEVBQUUsWUFBWSxrQkFBa0Isa0JBQWtCLElBQUk7QUFFNUQsU0FBTztBQUFBLElBQ0wsTUFBTSxvQkFBb0I7QUFBQSxJQUMxQixTQUFTO0FBQUEsTUFDUCxJQUFJO0FBQUEsTUFDSixZQUFZO0FBQUEsTUFDWixPQUFPO0FBQUEsTUFDUCxXQUFXO0FBQUEsUUFDVCxTQUFTLENBQUMsT0FBTyxZQUFZO0FBQUEsUUFDN0IsS0FBSztBQUFBLE1BQ1AsQ0FBQztBQUFBLE1BQ0QsV0FBVztBQUFBLFFBQ1QsV0FBVyxDQUFDLGdCQUFnQixDQUFDO0FBQUEsUUFDN0IsS0FBSztBQUFBLE1BQ1AsQ0FBQztBQUFBLE1BQ0QsaUJBQWlCO0FBQUEsUUFDZixRQUFRO0FBQUEsUUFDUixRQUFRO0FBQUEsVUFDTixNQUFNO0FBQUEsWUFDSixPQUFPO0FBQUEsVUFDVDtBQUFBLFFBQ0Y7QUFBQSxNQUNGLENBQUM7QUFBQTtBQUFBLE1BRUQsaUJBQWlCO0FBQUE7QUFBQSxNQUVqQixZQUFZO0FBQUE7QUFBQSxNQUVaLGNBQWM7QUFBQSxJQUNoQjtBQUFBLElBQ0EsU0FBUztBQUFBLE1BQ1AsT0FBTztBQUFBLFFBQ0wsS0FBS0MsTUFBSyxRQUFRLFFBQVEsSUFBSSxHQUFHLEtBQUs7QUFBQSxRQUN0QyxLQUFLQSxNQUFLLFFBQVEsUUFBUSxJQUFJLENBQUM7QUFBQSxNQUNqQztBQUFBLElBQ0Y7QUFBQSxJQUNBLFFBQVE7QUFBQSxNQUNOLE1BQU07QUFBQSxNQUNOLE1BQU07QUFBQSxNQUNOLE1BQU07QUFBQSxNQUNOLE9BQU87QUFBQSxRQUNMLFFBQVE7QUFBQSxVQUNOLFFBQVE7QUFBQSxVQUNSLGNBQWM7QUFBQSxVQUNkLFNBQVMsQ0FBQ0EsVUFBU0EsTUFBSyxRQUFRLElBQUksT0FBTyxPQUFPLEdBQUcsRUFBRTtBQUFBLFVBQ3ZELFFBQVE7QUFBQSxVQUNSLFdBQVcsQ0FBQyxPQUFPLFlBQVk7QUFFN0Isa0JBQU0sR0FBRyxZQUFZLENBQUMsVUFBVSxRQUFRO0FBQ3RDLHVCQUFTLFFBQVEsWUFBWSxJQUFJLElBQUksSUFBSSxJQUFJLE9BQU8sSUFBSSxRQUFRLE1BQU0sR0FBRyxRQUFRO0FBQUEsWUFDbkYsQ0FBQztBQUFBLFVBQ0g7QUFBQSxRQUNGO0FBQUEsTUFDRjtBQUFBLElBQ0Y7QUFBQSxJQUNBLE9BQU87QUFBQSxNQUNMLHVCQUF1QjtBQUFBO0FBQUEsSUFDekI7QUFBQSxFQUNGO0FBQ0YsQ0FBQzsiLAogICJuYW1lcyI6IFsicGF0aCIsICJwYXRoIl0KfQo=
