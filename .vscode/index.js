const fs = require('fs')
const path = require('path')

// 注释的正则表达式
// const commentPattern = /\/\*{34}[\s\S]*?Ronnie Zhang[\s\S]*?\*{34}\//

// 项目文件夹路径
const projectPath = 'src'

function removeCommentsFromFile(filePath) {
  // fs.readFile(filePath, 'utf8', (err, data) => {
  //   if (err) {
  //     console.error(`Error reading file ${filePath}:`, err)
  //     return
  //   }

  //   // 删除匹配的注释
  //   const newData = data.replace(commentPattern, '').trim()

  //   fs.writeFile(filePath, newData, 'utf8', (err) => {
  //     if (err) {
  //       console.error(`Error writing file ${filePath}:`, err)
  //     } else {
  //       console.log(`Processed file ${filePath}`)
  //     }
  //   })
  // })

  fs.readFile(filePath, 'utf8', (err, data) => {
    if (err) {
      console.error(`Error reading file ${filePath}:`, err)
      return
    }

    if (data.includes('Ronnie Zhang')) {
      const lines = data.split('\n')
      const newData = lines
        .slice(data.includes('***************************') ? 7 : 8)
        .join('\n')
        .trim()

      fs.writeFile(filePath, newData, 'utf8', (err) => {
        if (err) {
          console.error(`Error writing file ${filePath}:`, err)
        } else {
          console.log(`Processed file ${filePath}`)
        }
      })
    }
  })
}

function processDirectory(directory) {
  fs.readdir(directory, (err, files) => {
    if (err) {
      console.error(`Error reading directory ${directory}:`, err)
      return
    }
    files.forEach((file) => {
      const filePath = path.join(directory, file)
      fs.stat(filePath, (err, stats) => {
        if (err) {
          console.error(`Error getting stats for file ${filePath}:`, err)
          return
        }

        if (stats.isDirectory()) {
          processDirectory(filePath)
        } else if (stats.isFile() && /\.(vue|js|css|html)$/.test(filePath)) {
          removeCommentsFromFile(filePath)
        }
      })
    })
  })
}

processDirectory(projectPath)
